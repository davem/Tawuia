/**
 * 
 * @author dmyers Developed by me whilst working at APHP, H�pital Saint-Louis
 *         D�partement Biostatistique et Informatique M�dicale. Contact:
 *         david.myers@univ-paris-diderot.fr (whilst at Saint-Louis)
 *         david.myers.scibearspace@gmail.com
 * 
 */


package org.scibearspace.org.utils.calendar;

//import scibearspace.org.utils.logging.*;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import scibearspace.org.utils.logging.*;

/**
 * the tawuiaCalendar is a simple implementation of the java.util.Calendar object
 * 
 * It overrides the 2 constructors with calls to the constructor in the superClass.
 * 
 * Those methods that are stipulated for implementation are implemented to output a simple
 * error message. The functionality will be added as the needs arise.
 * 
 * @author dmyers
 *
 */

public class tawuiaCalendar extends Calendar {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final TawuiaLogger errLog = new TawuiaLogger("TawuiaLogger.Calendar");

	
	
	
	public tawuiaCalendar() {
		super();
	}
	
	public tawuiaCalendar(TimeZone zone, Locale aLocale) {
		super(zone, aLocale);
	}

	@Override
	public void add(int field, int amount) {

		errLog.add(1, this.getErrMessage("this method add is not yet implemented"), true);
		
	}

	@Override
	protected void computeFields() {
		errLog.add(1, this.getErrMessage("this method computeFields is not yet implemented"))	;	
	}

	@Override
	protected void computeTime() {
		errLog.add(1, this.getErrMessage("this method computeTime is not yet implemented"), true);
		}

	@Override
	public int getGreatestMinimum(int field) {
		errLog.add(1, this.getErrMessage("this method getGreatestMinimum is not yet implemented"))	;	
		return 0;
	}

	@Override
	public int getLeastMaximum(int field) {
		errLog.add(1, this.getErrMessage("this method getLeastMaximum is not yet implemented"))	;	
		return 0;
	}

	@Override
	public int getMaximum(int field) {
		errLog.add(1, this.getErrMessage("this method getMaximum is not yet implemented"))	;	
		return 0;
	}

	@Override
	public int getMinimum(int field) {
		errLog.add(1, this.getErrMessage("the method <getMinimum> is not yet implemented"))	;	
		return 0;
	}

	@Override
	public void roll(int field, boolean up) {
		errLog.add(1, this.getErrMessage("the method <roll> is not yet implemented"))	;	
		
	}



	/**
	 *a helper method to create the error message creator
	 *@ param m the extra message if any, 
	 *@return s the full message
	 */
	private String getErrMessage(String m) {
		//the third element in the current stact trace should be the calling method
		StackTraceElement currentLine = Thread.currentThread().getStackTrace()[2];
		StackTraceElement caller = Thread.currentThread().getStackTrace()[3];
		String s = m + "\ncalled from line " + currentLine.getLineNumber()
				+ " of method: " + this.getClass() + "."
				+ currentLine.getMethodName() + "\ncalled from line "
				+ caller.getLineNumber() + " of method: " + this.getClass()
				+ "." + caller.getMethodName();
		return s;
	}
}//end class