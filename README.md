Tawuia.

Tawui started out as a wrapper around log4j, for easier logging in my personal project.
It has since migrated into a collection of libraries for various functionalitites, taking on parts of other projects that I have created
and incorporated the library part of them.

This has begun to include : 
Database utilities.
HTML / XML parsing (will be added soon).
text processing.
GUI message system.

Each of these where originally developped independently and are gradually being moved over to a new version of the same projects but
build using maven rather than ANT.

This all came about with the release of the new log4j2 ~ which broke a lot of the functionality of the original project.