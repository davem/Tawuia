package scibearspace.org.utils.logging;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.logging.LogManager;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.spi.LoggerContext;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TawuiaLoggerTest {
	
	//class members
	static TawuiaLogger testLogger;


	
	
	
	/**
	 * All of the tests require an object of the logger to be created.
	 * So I create it in the @BeforeClass annotation.
	 * 
	 * Notice however that I also call one of the test methods from here, because if this test fails 
	 * then so will all the others.
	 * @return 
	 * 
	 * @throws Exception
	 */
	//@BeforeClass
	public static void setUP(){
		testLogger = new TawuiaLogger("testLogger");
	}
	
	
	//@AfterClass
	public static void cleanUp(){
		testLogger = null;
	}
	
	@Test
	//public static void setUpBeforeClass() throws Exception {
	public void LoggingTest() throws Exception {
	//create an object for testing.
		testLogger = new TawuiaLogger("testLogger");

			System.out.println("\n\n log4jproperties for logging are : \n" + PropertiesUtil.getProperties().toString());
			
					//LoggerContext lContect = org.apache.logging.log4j.LogManager.getContext();
					//Field f = lContect.getClass().getDeclaredField("configuration");
					//f.setAccessible(true);
					//XmlConfiguration iWantThis = (XmlConfiguration) f.get(lContect);
					//System.out.println("Config File: \n\n" + lContect.getClass().toString());//iWantThis.getName());		
			
		if(testLogger instanceof TawuiaLogger ){
			//we can continue
			testLogger.add(1, "continuing tests");
			assertTrue(true);//test passed
		}
		else{
			fail("building the class fails, tests cannot run");
		}
	}


	/**
	 * Test the tawuaiLogger default constructor works as expected
	 * from outside
	 * @return 
	 */
	@Test
	public void testLogOutput() {
		
		//the constructor send to STDout, so we need to capture that somehow for testing.
		
		ByteArrayOutputStream testSTDout = new ByteArrayOutputStream();
		//catch the value in the output stream
		//String OutputTest = testSTDout.toString();
		
		System.setOut(new PrintStream(testSTDout));
		
		//refresh the test classes object otherwise we can't capture the standardised output.
		TawuiaLogger quicTestLogger = new TawuiaLogger("quicTestLogger");
		
		
		
		if (testSTDout.toString().contains("Just testing a log message with priority set to INFO")
			 &&	testSTDout.toString().contains("Just testing a log message with priority set to WARN")
			 &&	testSTDout.toString().contains("Just testing a log message with priority set to ERROR")){
			assertTrue(true);
		}
		else {
			fail("error in creating a logger in the tawuiaLogger library");
		}
		
	}
	

	/**
	 * tests to ensure the supplemental static logger are functioning as expected.
	 */
	@Test
	public void testSciBearSpaceLogger(){
		//TawuiaLogger errLog = new TawuiaLogger("testingLogger");
		TawuiaLogger.topError(1, "Testing the SciBearSpace logger", false);
		
		assertTrue(TawuiaLogger.SciBearSpace instanceof Logger);
		
	}
	@Test
	public void testDevError(){
		//TawuiaLogger errLog = new TawuiaLogger("testingLogger");
		TawuiaLogger.setDevError(1, "Testing the SciBearSpace logger", false);
		
		assertTrue(TawuiaLogger.DevLog instanceof Logger);
		
	}
}//endClass