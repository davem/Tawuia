/**
 * 
 * @author dmyers Developed by me whilst working at APHP, Hôpital Saint-Louis
 *         Département Biostatistique et Informatique Médicale. Contact:
 *         david.myers@univ-paris-diderot.fr (whilst at Saint-Louis)
 *         david.myers.scibearspace@gmail.com
 * 
 */

package scibearspace.org.utils.logging;

//import scibearspace.org.utils.calendar.*;

//imports here


/**
 * The tawuiaException is the internal exception class that can be thrown
 * as a way of sending messages to a GUI and demanding a response.
 * 
 * It is part of the TawuiaLogger package, as such you get this and the logging
 * all in the same jar.
 * 
 * @author davem
 *
 */
public class TawuiaException extends Throwable{

//Static variables
/** The required serialID for this exception class */
private static final long serialVersionUID = 1L;

//the logger instance
private static TawuiaLogger errLog = new TawuiaLogger("tawuiaException");



//class variables / members
/** the error message */
private String msg;

/** It is envisaged that sometimes we may want to alter the value of 
 * a switch, this enables us to do so */
private boolean rtnVal;

/**
 * default constructor for this user defined exception class
 */
public TawuiaException(){
	//empty default constructor
}

/** 
 * Constructor that takes a simple String that is the message for this
 * error.
 * 
 * @param e The error message
 */
public TawuiaException(String e) {
	super(e);
	
}//end constructor / main method

/**
 * When catch is called we simply return the error message
 */
public String toString(){
	return super.getMessage();
	
}

/**
 * however if the boolean form of the constructor is called we should return the value of the
 * boolean value.
 * Setting the appropriate boolean value should be handled within the GUI, this to string method will 
 * report an error message to that is equal to the one that was sent originally.
 */

public boolean getResponse(boolean confirm){
	
	if (confirm = true) {
		errLog.add(1, this.msg + " may are going to continue...");
		return this.rtnVal;	
	}
	
	return false;
	
}

/**
 * Initially in the gui we should have a choice, if the choice (ok:Cancel) comes back as true (ok)
 * we should then throw out a confirm dialogue (for a double check)
 * @param b the value that we should return to the calling method
 */
public void setRtnVal(boolean b){
	this.rtnVal = b;
}
}//end class