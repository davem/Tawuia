package scibearspace.org.utils.logging;

import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

//import org.apache.logging.log4j.simple.SimpleLoggerContext;
//import org.apache.logging.log4j.api.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
//import org.apache.logging.log4j.spi.*;

public class TawuiaLogger {
	
	
	//class members / instance variables

	//create a category for our logger
	//we then use the cat member to get any logging messages output.
	//private static Category cat = Category.getInstance(tawuiaLogger.class.getName() );
	//a logger item to hold the messages
	
	private static org.apache.logging.log4j.Logger Log;
	public static org.apache.logging.log4j.Logger SciBearSpace;//a general error message type logger
	public static org.apache.logging.log4j.Logger DevLog;//a logger that I use when in development (in place of sysout)


	//for configuring our logger we create a definite xml config file
	//private XmlConfiguration xmlConfig;	
		
		private void init() {

			//this is the basic config from within
//			BasicConfiguration.doConfigure();
	          LoggerContext context = (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
	          File file = new File("resources/log4j2.xml");
	 
	          // this will force a reconfiguration
	          context.setConfigLocation(file.toURI());
		//this starts a few logs directly off to the logging location.
	        System.out.println(file.getAbsolutePath());
	          
			//PropertyConfigurator.configure("log4j.properties");//this sets the use of the log4j.properties file
			Log = LogManager.getLogger(TawuiaLogger.class.getName());
			//get log info, uncomment if testing directly from this class	
			Log.trace("Start of main()");
			Log.info("Just testing a log message with priority set to INFO");
			Log.warn("Just testing a log message with priority set to WARNING");
			
			Log.error("Just testing a log message with priority set to ERROR");
			//Log.fatal("Just testing a log message with priority set to FATAL");
			
		//during production I use the following line to change the logging level
			//log.log(Priority.DEBUG, "calling init()");
			//the options are DEBUG INFO WARN ERROR and FATAL
			

			
	}//end main

		
	/**
	 * public default constructor to create an error logger.
	 * 
	 * When logging is started using the tawuiaLogger as a library it will report the class that created it
	 * and the date of creation
	 * 
	 * @param name the name (or rather the class) associated with the logger
	 * 				
	 * @using:
	 * 
	 * In your class file add the following line
	 *
	 *	private static TawuiaLogger.tawuiaLogger errLog = new tawuiaLogger("<ClassName>");
	 *	
	 *	errLog : is the name of the logger, 
	 *  <ClassName> : This should be an identifying name for the class, this will enable you to 
	 *  			  identify where your error has occurred.
	 * 
	 * 
	 * An extra utility is also the getErrorMessage(String m) method.
	 * 
	 * This will generate details of the calling class, method, line number etc when used.
	 * 
	 * 
	 * 
	 * 
	 */

	public TawuiaLogger(String name)
	{	
		//set the config file location
		//xmlConfig = getConfig();
		this.init();
		
		//ensure that any copies of the logger are using the same system logger.
		//LoggerContext lc = new LoggerContext("lc");
		
		//AbstractMessageFactory mf = new  AbstractMessageFactory():
				
		this.Log = LogManager.getLogger(name);//this gives the specific name for the logger of this package
		
		this.startLoger();//create a few logging messages to confirm everything is working
		//log4j.configFile("log4j2.xml");
		
		
		//PropertyConfigurator.configure("log4j.properties");//set the properties file to use
		Calendar c = GregorianCalendar.getInstance();//add a calendar to the string
		String s = Thread.currentThread().getStackTrace().toString();
		Log.debug( s + " logging started " + c.get(5) + c.get(2) + c.get(1));
		//this initialises my topLevel logger
		SciBearSpace = LogManager.getLogger(TawuiaLogger.class.getName());
		
		//this is my specific dev logger
		DevLog = LogManager.getLogger(TawuiaLogger.class.getName());
	}


		
		
		
		
	/**
	 * A set of helper methods to determine the message being sent to the debugger
	 */

	/**
	 * 
	 * Add a message to the log,
	 * 
	 * @param i used in the switch|case to set the required level of debug
	 * @param s the message to send
	 * 
	 * @see add(int i, string s, boolean withTrace) : for getting the full stack trace from the message
	 * 
	 * use this method to create a log message of any required level
	 * The levels are as follows:
	 * 1 	debug - used during production
	 * 2	info  - may be used for messages to user
	 * 3	warn  - warnings for requesting users re-enter information (data type checking etc)
	 * 4	error - programmatic / user error may result in unexpected output 
	 * 5	fatal -	severe error (comms error) the system will terminate, cleanly if possible, current state will be written to file
	 * 
	 * Prior to all this however the method determines the calling class of itself
	 * 
	 */

	public void add(int i, String s)
	{
		
		
		
		
		switch (i)
		{
		case 1: Log.debug(s);break;//debug, not turned on in production, can be if required, see config file
		case 2: Log.info(s);break;//send message to user, no interaction required, no message to log unless required
		case 3: Log.warn(s);break;//message and response required, log not required.
		case 4: Log.error(s);break;//message, response, and log of all info passing
		
		//this will kill the app
		case 5: Log.fatal(s);
			this.Log.fatal("message received from class to die, see previous message for info.");
		break;//send a message, log to file, and quit app.
		}
	}

	/**
	 * overloaded add method to create a trace from the included switch
	 * @param i used in the switch to create the required level of debug
	 * @param s the message to send
	 * @param withTrace if true will also collect and print a full stack trace.
	 * 					if false, will simple call the default add(i, s) method.
	 * 
	 * @see #add(int, String) for mor info on logging levels.
	 */

	public void add(int i, String s, boolean withTrace)
	{
		
		if(withTrace)
		{
		this.add(i, s + "here is the full trace\n" + getTrace() );
		}
		else
		{
			this.add(i, s);
		}
	}

	/**
	 * utility to get the full stack trace
	 * @return the stack trace, one element per line.
	 */
	private static String getTrace()
	{
		String el = new String();
		for (StackTraceElement ste: Thread.currentThread().getStackTrace())
		{
			el +="\n"+ste;
		}
		return el;
	}//end getTrace

	private void startLoger()
	{
		Log.debug("Starting logging for class...");
		Log.info("Just testing a log message with priority set to INFO");
		Log.warn("Just testing a log message with priority set to WARN");
		Log.error("Just testing a log message with priority set to ERROR");
		
		//Doing the following line just kills the app! 
		//log.fatal("Just testing a log message with priority set to FATAL");
	}

	/**
	 * A top level error log for setting a message for various class casts,
	 * should really be handled in the code so this should all but never get called
	 * @param l the level of the message
	 * @param m the message.
	 * @param withTrace if true will also collect and print a full stack trace.
	 * 					if false, will simple call the default add(i, s) method.
	 * 
	 * @info: This is used inside try catch blocks to catch those events that are
	 * truly 'exceptional' but ones that we have no real control over (such as class casts
	 * mentioned above), or other compiler or JVM errors
	 * 
	 */
	public static void topError(int i, String s, boolean withTrace)
	{
		String msg = s;
		if(withTrace)
		{
			msg += getTrace(); 
		}
		
		//SciBearSpace = new tawuiaLogger("SciBearSpace");
		switch (i)
		{
		case 1: SciBearSpace.debug(msg);break;//for general errors we don't care about but must be caught
		case 2: SciBearSpace.info(msg);break;//error created by comms with DB or file, may require a response from user
		case 3: SciBearSpace.warn(msg);break;//anything above 3 should always be logged to a file or DB
		case 4: SciBearSpace.error(msg);break;//log to file and send message to user about error
		
		case 5: SciBearSpace.fatal(msg);break;//send message to user, log to file, close app!
		}
	}

	/**
	 * this is the DevError logger, it is used in place of System.out.printline for
	 * sending messages to the console and/or chainsaw
	 * 
	 * During development we log ALL events going out, once released into a 'production'
	 * level the logging is silenced for anything below level 3.
	 * 
	 * Level 3 errors can be used for mouse over popups etc, enable user to turn
	 * these on or off
	 * 
	 * Anything above level 4 should most likely also send the same info into
	 * the KittyMessagingSystem, where it can be dealt with by the user.
	 * 
	 * @param i the logging level ({@link #add(int, String)})
	 * @param s the message being reported.
	 * @param withTrace if true will also collect and print a full stack trace.
	 * 					if false, will simple call the default add(i, s) method.
	 * 
	 */
	public static void setDevError(int i, String s, boolean withTrace)
	{
		String msg = s;
		if(withTrace)
		{
			msg += getTrace(); 
		}
		
		
		switch (i)
		{
		case 1: DevLog.debug(msg);break;//should be used for debug messages during development only
		case 2: DevLog.info(msg);break;
		//TODO use a level of 3 for creating mouse over popups
		case 3: DevLog.warn(msg);break;
		//TODO add a call to the KMS to deal with errors above case 4
		case 4: DevLog.error(msg);break;
		//TODO level 5 and above should send a message via KMS before
		//closing connections etc, and putting the message into a text file
		//for viewing by the user or sending it automatically to a developer
		case 5: DevLog.fatal(msg);break;
		}
	}	
	
	/**
	 *a helper method to create the error message creator
	 *@ param m the extra message if any, 
	 *@return s the full message
	 *
	 *This method extracts details of the current class, method and line from the
	 *stack trace. It also extracts the same detail of the class that was instrumental
	 *calling this class.
	 *
	 */
	public String getErrMessage(String m) {
		//the third element in the current stact trace should be the calling method
		StackTraceElement currentLine = Thread.currentThread().getStackTrace()[2];
		StackTraceElement caller = Thread.currentThread().getStackTrace()[3];
		String s = m + "\ncalled from line " + currentLine.getLineNumber()
				+ " of method: " + this.getClass() + "." + currentLine.getMethodName() 
				+ "\ncalled from line " + caller.getLineNumber()
				+ " of method: " + this.getClass() + "." + caller.getMethodName();
		return s;
	}

}//end class
